<?php

define('COSMOS_URL', 'YOUR_CLOUD_URL');
define('CLIENT_ID', 'YOUR_CLIENT_ID');
define('CLIENT_SECRET', 'YOUR_CLIENT_SECRET');
define('APP_URL', 'YOUR_APP_URL');

function cosmos_req($url, $data = array(), $isPost = false, $oauth = null)
{
    $handle = curl_init(COSMOS_URL . '/v1' . $url);

    if ((!isset($oauth) || $oauth == null) && isset($_COOKIE['token'])) {
        $oauth = $_COOKIE['token'];
    }

    $headers = array();

    if (isset($oauth)) {
        $headers[] = 'Authorization:Bearer ' . $oauth;
    }

    if ($isPost) {
        $data_string = json_encode($data);

        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Content-Length: ' . strlen($data_string);

        curl_setopt($handle, CURLOPT_POST, $isPost);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data_string);
    }

    curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

    $resp = curl_exec($handle);

    return json_decode($resp);
}

?>
