<?php

//This file opens an Oauth 2.0 login window

if (isset($_COOKIE['token'])) {
    header('Location: ' . '/index.php');
    die();
}

include_once './cosmos.php';

?>
<html lang="en">
<head>
</head>
<body>

<h3>Log In</h3>
<div id="login-error" style="display: none;">There was an error logging in.</div>

<button id="login-btn">Log In with your LMS</button>

<script>
    var COSMOS_URL = '<?php echo COSMOS_URL ?>';

    //Attach a listener to the login button

    document.getElementById('login-btn').addEventListener('click', function () {

        //Create the oauth login url
        //See https://docs.atlas.school/docs/en/api-authentication/ for information and options

        var redirect = window.location.origin + '/redirect.php';
        var url = COSMOS_URL + "/v1/oauth2/auth?response_type=code&redirect_uri=" + encodeURIComponent(redirect);

        var popup = window.open(url, 'Login', 'location=0,status=0,width=950,height=650');

        //Check if the popup has succeeded in an interval

        var int = setInterval(function () {
            try {
                if (popup.closed) {
                    clearInterval(int);
                }
                if (popup.location.toString().includes('login_success')) {
                    popup.close();
                    setTimeout(function () {
                        //Redirect the user to the main application
                        window.location = '/index.php';
                    }, 1000);
                    clearInterval(int);
                } else if (popup.location.toString().includes('login_error')) {
                    document.getElementById('login-error').setAttribute('style', '');
                    clearInterval(int);
                }
            } catch (e) {
                console.log(e);
            }
        }, 100);

    })
</script>
</body>
</html>
