<?php

if (!isset($_COOKIE['token'])) {
    header('Location: ' . '/login.php');
    die();
}

require_once "./cosmos.php";

//The user's data could be synchronized and stored in your own database.
//Here, we are saving the Atlas token and making the request to the LMS every time the user refreshes the page.

$user = cosmos_req("/users/me");
$courses = cosmos_req('/courses');

if (isset($courses->error)) {
//    User logged in with a non-lms login method, such as Microsoft or Google Apps for Education
    $courses = array();
}

?>
<html lang="en">
<head>
</head>
<body>
<h3>App Home</h3>
<?php echo $user->first . ' ' . $user->last ?><br/>
<?php echo $user->lms ?><br/>
<a href="/logout.php">Log Out</a>

<?php if (!empty($courses)) { ?>
    <h2>My Courses</h2>
<?php } ?>
<?php foreach ($courses as $course) { ?>
    <div><?php echo $course->title; ?>
        <form action="/assign.php" method="post">
            <input type="hidden" name="courseId" value="<?php echo $course->id ?>"/>
            <input type="text" placeholder="URL" name="url"/>
            <button type="submit">Assign Resource</button>
        </form>
    </div>
<?php } ?>
</body>
</html>
