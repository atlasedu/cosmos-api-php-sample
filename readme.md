## Cosmos API - PHP Sample

### Instructions
* Replace the variables in **cosmos.php** with connection details to your Cosmos API server
* Create an Apache or PHP-FPM server to serve the files in the directory
* Visit **/index.php** in your browser
* Log in with a teacher account to an LMS of your choice

### Walkthrough

Cosmos provides a unified API to access multiple LMSs for single-sign-on and course object management.
This simple application demonstrates the login flow, accessing privileged resources for the user, and
assigning to a course.

Login follows a standard OAuth 2 flow.
Javascript opens a login window and monitors for the status to know when to redirect to the main application
or show an error.

**cosmos.php** includes configuration variables and a simple function that simplifies CURL requests. You may
use whatever method you choose for communicating with the Cosmos API server.

The **redirect.php** script is set as the redirection handler, to exchange the token for an access code.
* Inside of redirect.php, code is commented out where you could retrieve the user's information and then match
	them to a user in your system or create a user account for them. In this sample, the token is used directly.

Once a user has signed in an a Cosmos access token has been retrieved, the user is redirected to the main **index.php** page.

The **index.php** page makes two calls to Cosmos, to retrieve the current user and their courses. A list
of courses is rendered to the page, with a form for each to make a *POST* request to **assign.php**.

**assign.php** reads the form *POST* variables, and makes a *POST* request to the Cosmos API server to create a resource.

**logout.php** simply unsets the *token* cookie used as a session marker and redirects the user to **login.php**.
