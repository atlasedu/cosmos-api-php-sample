<?php

if (!isset($_COOKIE['token']) || $_SERVER['REQUEST_METHOD'] != 'POST') {
    http_response_code(400);
    echo "Must be logged in.";
    die();
}

require_once './cosmos.php';

$resourceUrl = $_POST['url'];
$courseId = $_POST['courseId'];

$data = array(
    "title" => $resourceUrl,
    "resourceUrl" => $resourceUrl,
    "resourceType" => "assignment"
);

$result = cosmos_req("/courses/" . $courseId . "/resources", $data, true);

if (isset($result->error)) {
    http_response_code(400);
    echo "Invalid request.";
    die();
}

?>

<html lang="en">
<head><title>Resource Assigned</title></head>
<body>
<?php if (isset($result->error)) { ?>
    <p>There was an error assigning your resource.</p>
<?php } else { ?>
    <p>Your resource (<?php echo $url; ?>) has been assigned.</p>
<?php } ?>

<a href="/index.php">Return</a>

</body>
</html>



