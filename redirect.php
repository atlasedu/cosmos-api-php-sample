<?php

if (!isset($_GET['code'])) {
    //Error with login - probably that the user cancelled the process.
    header('Location: ' . '/login.php?login_error=true');
    die();
}

require_once "./cosmos.php";

//Format the OAuth token request
//For more information, see https://docs.atlas.school/docs/en/api-authentication/

$data = array(
    'grant_type' => 'authorization_code',
    'client_id' => CLIENT_ID,
    'client_secret' => CLIENT_SECRET,
    'redirect_uri' => APP_URL . '/redirect.php',
    'code' => $_GET['code']);

$resp = cosmos_req('/oauth2/token', $data, true);

if (isset($resp->access_token)) {
    $tkn = $resp->access_token;

    //$tkn can be used to make requests as this user to their LMS

//    //Get the user details from Cosmos
//    $user = cosmos_req("/users/me", array(), false, $tkn);
//
//    //Check if the user exists in the system. If not, create their account.
//
//    if(user_exists($user->email)){
//       //log in existing user
//    } else {
//       //create new user here
//    }

    //For our simple scenario, we will just use the atlas token as our login token.
    setcookie("token", $tkn, time() + 31536000, '/', '', false, true);

    //Redirect the page to ?login_success=true, which will cause the main page's javascript to close the popup.
    header('Location: ' . '/index.php?login_success=true');
} else {
    //Error exchanging token.

    //Redirect the page to ?login_error=true, which will cause javascript to close the
    //popup and show the error message
    header('Location: ' . '/index.php?login_error=true');
}

?>
